//
//  Profile.swift
//  Skilla
//
//  Created by Pau Lleonart Calvo on 12/3/20.
//  Copyright © 2020 SkillaTeam. All rights reserved.
//

import SwiftUI

let jobCategories = ["Sales", "Product", "Business", "Data", "DevOps / Sysadmin", "Finance / Legal", "Human Resources","QA", "Teaching", "Writing", "Medical / Health", "All others"]

struct Profile: View {
    
    @EnvironmentObject var userData: UserData
    
    // Field Entries
    @State private var age = ""
    @State private var email = ""
    @State private var phone = ""
    @State private var country = ""
    @State private var state = ""
    @State private var city = ""
    @State private var name = ""
    @State private var hook = ""
    @State private var bio = ""
    @State private var otherInfo = ""

    
    @State private var experienceCount = 1
    @State private var experienceKeysList =  ["", "", "", "", ""]
    @State private var experienceValuesList =  ["", "", "", "", ""]
    
    @State private var educationCount = 1
    @State private var educationKeysList = ["", "", "", "", ""]
    @State private var educationValuesList = ["", "", "", "", ""]

    
    let colors = [Color.red, Color.purple, Color.pink]
    
    @State private var showChangesAppliedAlert = false
    @State private var selectedIndex = UserDefaults.standard.integer(forKey: "Index")
        
    var body: some View {
        NavigationView{
            ZStack{
                LinearGradient(gradient: Gradient(colors: colors), startPoint: .topLeading, endPoint: .bottomTrailing)
                    .edgesIgnoringSafeArea(.all)
                VStack{
                    
                    if(userData.userContactInformation.profilePic == Image(systemName: "person.circle")) {
                        userData.userContactInformation.profilePic
                            .resizable()
                            .aspectRatio(contentMode: .fit)
                            .frame(minWidth: 75, maxWidth: 100)
                            .cornerRadius(15)
                            .padding(.horizontal, 30)
                            .padding(.bottom, 10)
                            .foregroundColor(.white)
                            .shadow(radius: /*@START_MENU_TOKEN@*/10/*@END_MENU_TOKEN@*/)
                            .padding(.bottom, 10)
                    }
                    else {
                        userData.userContactInformation.profilePic
                            .resizable()
                            .aspectRatio(contentMode: .fit)
                            .frame(minWidth: 75, maxWidth: 100)
                            .cornerRadius(15)
                            .overlay(RoundedRectangle(cornerRadius: 15)
                                        .stroke(Color.white, lineWidth: 3))
                            .padding(.horizontal, 30)
                            .padding(.bottom, 10)
                            .shadow(radius: /*@START_MENU_TOKEN@*/10/*@END_MENU_TOKEN@*/)
                            .padding(.bottom, 10)
                    }
                    
                    
                    Form {
                        Button(action:{
                            UserDefaults.standard.set(jobCategories[selectedIndex], forKey: "Category")
                            UserDefaults.standard.set(selectedIndex, forKey: "Index")
                            searchFromJobsApi(category: jobCategories[selectedIndex])
                            userData.listOfJobs = jobsList
                            
                            /*
                             
                             */
                            
                            showChangesAppliedAlert = true
                            
                            userContact.name = self.name.isEmpty ? userContact.name : self.name
                            userContact.age = self.age.isEmpty ? userContact.age : self.age
                            userContact.email = self.email.isEmpty ? userContact.email : self.email
                            userContact.phone = self.phone.isEmpty ? userContact.phone : self.phone
                            userContact.city = self.city.isEmpty ? userContact.city : self.city
                            userContact.state = self.state.isEmpty ? userContact.state : self.state
                            userContact.country = self.country.isEmpty ? userContact.country : self.country
                            userContact.resume.bio = self.bio.isEmpty ? userContact.resume.bio : self.bio
                            userContact.resume.otherInfo = self.otherInfo.isEmpty ? userContact.resume.otherInfo : self.otherInfo
                            userContact.resume.hook = self.hook.isEmpty ? userContact.resume.hook : self.hook
                            userContact.profilePic = userData.userContactInformation.profilePic
                            
                            userContact.resume.experience = [String : [String: String]]()
                            userContact.resume.education = [String : [String: String]]()

                            for index in 0..<self.educationCount {
                                if (!educationKeysList[index].isEmpty && !educationValuesList[index].isEmpty) {
                                    let education = [
                                        "degree" : educationKeysList[index],
                                        "institution" : educationValuesList[index]
                                    ]
                                    userContact.resume.education["education\(index)"] = education
                                }
                            }
                            
                            for index in 0..<self.experienceCount {
                                
                                if (!experienceKeysList[index].isEmpty && !experienceValuesList[index].isEmpty) {
                                    let experience = [
                                        "position" : experienceKeysList[index],
                                        "organization" : experienceValuesList[index]
                                    ]
                                    userContact.resume.experience["experience\(index)"] = experience
                                }
                            }
                            print(self.experienceKeysList)
                            print(self.educationKeysList)

                            print(userContact.resume.experience)
                            print(userContact.resume.education)

                            
                            userData.userContactInformation = userContact
                            
                            publishUserInfoToDatabase(userInfo: userData.userContactInformation, userData: userData)
                            
                            
                        }) {
                            Text("Apply Changes")
                                .font(.headline)
                                .foregroundColor(.blue)
                                .frame(minWidth: 100, maxWidth: 600, alignment: .center)
                        }
                        
                        Section(header: Text("Selected your Preferred Job Category")) {
                            
                            // Picker: Part 3 of 3
                            Picker("Selected Category:", selection: $selectedIndex) {
                                ForEach(0 ..< jobCategories.count, id: \.self) {
                                    Text(jobCategories[$0])
                                }
                            }
                        }   // End of Section
                        .alert(isPresented: $showChangesAppliedAlert, content: { self.changesAppliedAlert })
                        Section(header: Text("Enter Your Profile Data")) {
                            VStack{
                                TextField("Name", text: $name)
                                Divider()
                                TextField("Age", text: $age)
                                Divider()
                                TextField("Phone", text: $phone)
                                Divider()
                                TextField("Email", text: $email)
                                Divider()
                                HStack{
                                    TextField("City", text: $city)
                                    Divider()
                                    TextField("State", text: $state)
                                    Divider()
                                    TextField("Country", text: $country)
                                }
                            }
                        }   // End of Section
                        
                        // End of Section
                        
                        Section(header: Text("Hook")) {
                            TextField("Enter short and interesting hook", text: self.$hook)
                        }
                        
                        //EDUCATION SECTION
                        Section(header:
                            HStack{
                                Text("My Education")
                                Spacer()
                                
                                Button(action: {
                                    if(self.educationCount < 5) {
                                        self.educationCount += 1

                                    }
                                }){
                                    Image(systemName: "plus.circle")
                                        .foregroundColor(.blue)
                                        .font(Font.body)

                                }
                                .padding(.trailing, 40)
                                
                                Button(action: {
                                    if(self.educationCount > 1) {
                                        self.educationCount -= 1

                                    }
                                }){
                                    Image(systemName: "minus.circle")
                                        .foregroundColor(.blue)
                                        .font(Font.body)
                                }
                            }
                        ) { //Content starts here
                            
                            VStack{
                                ForEach(0..<self.educationCount, id:\.self) {index in
                                    VStack {
                                        HStack {
                                            Text("Degree:")
                                            TextField("Enter Degree Achieved", text: $educationKeysList[index])
                                        }
                                        HStack {
                                            Text("Institution:")
                                            TextField("Enter Institution Attended", text: $educationValuesList[index])
                                        }
                                        
                                    }
                                    if(index != self.educationCount - 1 && self.educationCount > 1) {
                                        Divider()
                                    }
                                }
                            }
                        }
                        
                        Section(header:
                            HStack{
                                Text("My Experience")
                                Spacer()
                                
                                Button(action: {
                                    if(self.experienceCount < 5) {
                                        self.experienceCount += 1

                                    }
                                }){
                                    Image(systemName: "plus.circle")
                                        .foregroundColor(.blue)
                                        .font(Font.body)

                                }
                                .padding(.trailing, 40)
                                
                                Button(action: {
                                    if(self.experienceCount > 1) {
                                        self.experienceCount -= 1

                                    }
                                }){
                                    Image(systemName: "minus.circle")
                                        .foregroundColor(.blue)
                                        .font(Font.body)
                                }
                            }
                        ) { //Content starts here
                            
                            VStack{
                                ForEach(0..<self.experienceCount, id:\.self) {index in
                                    VStack {
                                        HStack {
                                            Text("Position:")
                                            TextField("Enter Position Title", text: $experienceKeysList[index])
                                        }
                                        HStack {
                                            Text("Organization:")
                                            TextField("Enter Employer", text: $experienceValuesList[index])
                                        }
                                        
                                    }
                                    if(index != self.experienceCount - 1 && self.experienceCount > 1) {
                                        Divider()
                                    }
                                }
                            }
                        }// end of Section
                        
                        Section(header: Text("Biography"), footer:
                            Button(action: {
                                self.dismissKeyboard()
                            }) {
                                Image(systemName: "keyboard")
                                    .font(Font.title.weight(.light))
                                    .foregroundColor(.blue)
                            }
                        ) {
                            TextEditor(text: self.$bio)
                                .frame(height: 100)
                                .font(.system(size: 14))
                                .foregroundColor(.primary)
                                .multilineTextAlignment(.leading)
                        }
                        
                        Section(header: Text("Other Info"), footer:
                            Button(action: {
                                self.dismissKeyboard()
                            }) {
                                Image(systemName: "keyboard")
                                    .font(Font.title.weight(.light))
                                    .foregroundColor(.blue)
                            }
                        ) {
                            TextEditor(text: self.$otherInfo)
                                .frame(height: 100)
                                .font(.system(size: 14))
                                .foregroundColor(.primary)
                                .multilineTextAlignment(.leading)
                        }
                        
                    }   // End of Form
                    .navigationBarTitle(Text("Profile"))
                    .shadow(radius: 10)
                    .navigationBarItems(
                        trailing:
                            HStack(spacing: 15) {
                                NavigationLink(destination: QRCodeView(contact: userData.userContactInformation)) {
                                    Image(systemName: "qrcode")
                                        .foregroundColor(.white)
                                        .font(.title2)
                                }
                                NavigationLink(destination: Setting()){
                                    Image(systemName: "gear")
                                        .foregroundColor(.white)
                                        .font(.title2)
                                    
                                }
                            }
                    )
                    
                } // End of VStack
            } // End of ZStack
        } // End of Navigation View
        .onAppear(perform: {
            
            /*
             Once the user's profile appears, we must populate it with either no content or specific content depending on their last entered data. We must map every piece of information to its proper text fields. So, we use an array of strings which we bind text fields to.
             */
            if (userData.userContactInformation.resume.education["None"] == nil) {
                self.educationCount = userData.userContactInformation.resume.education.count
                
                var index = 0
                for (_, value) in userData.userContactInformation.resume.education {
                    self.educationKeysList[index] = value["degree"]!
                    self.educationValuesList[index] = value["institution"]!
                    index += 1
                }
            }
            
            if (userData.userContactInformation.resume.experience["None"] == nil) {
                self.educationCount = userData.userContactInformation.resume.experience.count
                
                print("ATTEMPTING TO PARSE")
                print(userData.userContactInformation.resume.experience)
                var index = 0
                for (_, value) in userData.userContactInformation.resume.experience {
                    self.experienceKeysList[index] = value["position"]!
                    self.experienceValuesList[index] = value["organization"]!
                    index += 1
                }
            }
            
            /*
             set proper values
             */
            self.age = userData.userContactInformation.age == "Unavailable" ? "" : userData.userContactInformation.age
            self.email = userData.userContactInformation.email  == "Unavailable" ? "" : userData.userContactInformation.email
            self.phone = userData.userContactInformation.phone == "Unavailable" ? "" :userData.userContactInformation.phone
            self.country = userData.userContactInformation.country == "Unavailable" ? "" :userData.userContactInformation.country
            self.state = userData.userContactInformation.state == "Unavailable" ? "" :userData.userContactInformation.state
            self.city = userData.userContactInformation.city == "Unavailable" ? "" : userData.userContactInformation.city
            self.name = userData.userContactInformation.name == "Unavailable" ? "" : userData.userContactInformation.name
            self.hook = userData.userContactInformation.resume.hook == "Unavailable" ? "" : userData.userContactInformation.resume.hook
            self.bio = userData.userContactInformation.resume.bio == "Unavailable" ? "" : userData.userContactInformation.resume.bio
            self.otherInfo = userData.userContactInformation.resume.otherInfo == "Unavailable" ? "" : userData.userContactInformation.resume.otherInfo

        })
        .navigationViewStyle(StackNavigationViewStyle())
    }
    
    
    /*
     Dismisses keyboard for text editor
     */
    func dismissKeyboard() {
        UIApplication.shared.sendAction(#selector(UIResponder.resignFirstResponder), to: nil, from: nil, for: nil)
    }
    
    /*
     ----------------------------------
     MARK: - New Category Applied Alert
     ----------------------------------
     */
    var changesAppliedAlert: Alert {
        Alert(title: Text("Changes Applied!"),
              message: Text("Your Profile Has Been Updated!"),
              dismissButton: .default(Text("OK")) )
    }
    
}

struct Profile_Previews: PreviewProvider {
    static var previews: some View {
        Profile()
    }
}

