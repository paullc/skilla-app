//
//  JobsApiData.swift
//  Skilla
//
//  Created by Pau Lleonart Calvo on 12/3/20.
//  Copyright © 2020 SkillaTeam. All rights reserved.
//

import Foundation
import SwiftUI

// Global array of weather structs
var jobsList = [Job]()
var jobFound = Job(id: UUID(), jobUrl: "", title: "", companyName: "", jobCategory: "", jobType: "", publicationDate: "", salaryRange: "", description: "")

fileprivate var previousCategory = ""


/*
 *********************************************
 MARK: - Obtain Matching Jobs
 *********************************************
 */
public func searchFromJobsApi(category: String) {

    // Avoid executing this function if already done for the same category and query
    if category == previousCategory {
        return
    } else {
        previousCategory = category
    }

    // Initialize the array as an empty list
    jobsList = [Job]()

    let categoryQuery = category.replacingOccurrences(of: " ", with: "%20")

    let jobApiUrl = "https://remotive.io/api/remote-jobs?category=\(categoryQuery)&limit=30"


    // The function is given in UtilityFunctions.swift
    let jsonDataFromApi = getJsonDataFromApi(apiUrl: jobApiUrl)

    if jsonDataFromApi == nil {
        return
    }

    /*
     ******************************
     MARK: - Sample API data
     ******************************
     */
    /*
  {
      "0-legal-notice": "Remotive API Legal Notice",
      "job-count": 1, // Number or jobs matching the query == length of 'jobs' list
      "jobs": [ // The list of all jobs retrieved. Then for each job, you get:
                  {
                  "id": 123, // Unique Remotive ID
                  "url": "https://remotive.io/remote-jobs/product/lead-developer-123", // Job
                  listing detail url
                  "title": "Lead Developer", // Job title
                  "company_name": "Remotive", // Name of the company which is hiring
                  "category": "Software Development", // See
                  https://remotive.io/api/remote-jobs/categories for existing categories
                  "job_type": "full_time", // "full_time" or "contract" here. It's optional an often not filled.
                  "publication_date": "2020-02-15T10:23:26", // Publication date and time on
                  https://remotive.io
                  "candidate_required_location": "Worldwide", // Geographical restriction for the
                  remote candidate, if any.
                  "salary": "$40,000 - $50,000", // salary description, usually a yearly salary
                  range, in USD. It's optional an often not filled.
                  "description": "The full HTML job description here", // HTML full description of
                  the job listing
                  "company_logo_url": "..."
                  },
      ]
      }
        .
        .
        .
        ],
     .
     .
     .
     }
     */


    //------------------------------------------------
    // JSON data is obtained from the API. Process it.
    //------------------------------------------------

    do {
        /*
         Foundation framework’s JSONSerialization class is used to convert JSON data
         into Swift data types such as Dictionary, Array, String, Number, or Bool.
         */

        let jsonResponse = try JSONSerialization.jsonObject(with: jsonDataFromApi!,
                                                            options: JSONSerialization.ReadingOptions.mutableContainers)


        var jobQueryDataDictionary = Dictionary<String, Any>()

        if let jsonObject = jsonResponse as? [String: Any] {
            // Iterate over the array
            jobQueryDataDictionary = jsonObject
        } else{
            return
        }

        var jobsJsonArray = [Any]()
        if let jsonArray = jobQueryDataDictionary["jobs"] as? [Any]{
            jobsJsonArray = jsonArray
        } else{
            return
        }

        var jobsJsonObject = [String: Any]()

        for i in 0..<jobsJsonArray.count {

            if let jObject = jobsJsonArray[i] as? [String: Any]{
                jobsJsonObject = jObject
            } else {
                return
            }

            jobQueryDataDictionary = jobsJsonObject

            var jobUrl="", title="", companyName="", jobCategory="", jobType="", publicationDate="", salaryRange="", description=""

            // Obtain job link
            if let jobLink = jobQueryDataDictionary["url"] as? String{
                jobUrl = jobLink
            } else {
                return
            }

            // Obtain title
            if let jobTitle = jobQueryDataDictionary["title"] as? String{
                title = jobTitle
            } else {
                return
            }
            
            // Obtain companyName
            if let nameOfCompany = jobQueryDataDictionary["company_name"] as? String{
                companyName = nameOfCompany
            } else {
                return
            }
            
            // Obtain catagory
            if let category = jobQueryDataDictionary["category"] as? String{
                jobCategory = category
            } else {
                return
            }
            
            // Obtain job Type
            if let type = jobQueryDataDictionary["job_type"] as? String{
                jobType = type
            } else {
                return
            }
            
            // Obtain job publicationdate
            if let pubDate = jobQueryDataDictionary["publication_date"] as? String{
                publicationDate = pubDate
            } else {
                return
            }
            
            // Obtain job Salary Range
            if let salary = jobQueryDataDictionary["salary"] as? String{
                salaryRange = salary
            } else {
                return
            }
            // Obtain company HTML Description
            if let HTMLdescription = jobQueryDataDictionary["description"] as? String{
                description = HTMLdescription
            } else {
                return
            }
            
            if salaryRange == ""{
                salaryRange = "N/A"
            }
            
            let jobTypeStyled = jobType.replacingOccurrences(of: "_", with: " ")
            
            jobFound = Job(id: UUID(), jobUrl: jobUrl, title: title, companyName: companyName, jobCategory: jobCategory, jobType: jobTypeStyled, publicationDate: publicationDate, salaryRange: salaryRange, description: description)
            jobsList.append(jobFound)
        } // End of add for-loop

    } // End of do
    catch{
        // Weather vals will have the same inital values as above
        return
    }

}
