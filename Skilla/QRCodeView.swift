//
//  QRCodeView.swift
//  Skilla
//
//  Created by Pau Lleonart Calvo on 12/7/20.
//  Copyright © 2020 SkillaTeam. All rights reserved.
//

import SwiftUI

struct QRCodeView: View {
    @EnvironmentObject var userData: UserData
    
    let contact: Contact

    var body: some View {
        ZStack {
            
            /*
             Linear gradient background
             */
            LinearGradient(gradient: Gradient(colors:
                                                [.red, .purple]),
                                   startPoint: .top,
                                   endPoint: .bottomTrailing).edgesIgnoringSafeArea(.all)
            VStack {

                Text("\(contact.name.isEmpty ? "User" : contact.name)'s\n Skilla Code")
                    .foregroundColor(.white)
                    .font(Font.title.weight(.bold))
                    .multilineTextAlignment(.center)
                    .shadow(radius: 15)
                    .padding(.bottom, 20)
                
                HStack {
                    /*
                     Create barcode with professor balci's bacrode creator
                     */
                    Image(uiImage: createBarcode(from: contact.id, type: "CIQRCodeGenerator", scaleFactor: 8)!)
                        .cornerRadius(15)
                        .aspectRatio(contentMode: .fit)
                        .frame(width: 300)
                        .shadow(radius: 15)

                }
            }

        }
    }
    
}
