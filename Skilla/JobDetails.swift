//
//  JobDetails.swift
//  Skilla
//
//  Created by Pau Lleonart Calvo on 12/3/20.
//  Copyright © 2020 SkillaTeam. All rights reserved.
//

import SwiftUI
import WebKit

struct JobDetails: View {
    
    // Input Parameter
    let job: Job
    
    var descriptionView: WKWebView!
    let colors = [Color.pink, Color.purple, Color.red, Color.pink]
    
    var body: some View {
        
        VStack{
            ZStack(alignment: .leading) {
                Image("GradientBackground")
                    .resizable()
                    .aspectRatio(contentMode: .fill)
                    .frame(minWidth: 300, maxWidth: 600, minHeight: 150, maxHeight: 150, alignment: .center)
                VStack(alignment: .leading){
                    Text(job.title)
                        .font(.title2)
                        .fontWeight(.bold)
                    Text("Company: \(job.companyName)")
                        .font(.system(size: 19))
                        .fontWeight(.bold)
                        .foregroundColor(Color(UIColor.systemGray5))
                    Text("Category: \(job.jobCategory)")
                        .font(.headline)
                        .foregroundColor(Color(UIColor.systemGray5))
                    Text("Salary: \(job.salaryRange)")
                        .font(.headline)
                        .foregroundColor(Color(UIColor.systemGray5))
                    Text("Post Date: \(dateTimeformat(inputDate: job.publicationDate))")
                        .font(.headline)
                        .foregroundColor(Color(UIColor.systemGray5))
                }
                .foregroundColor(.white)
                .padding(12)
                
            }
            .clipShape(RoundedRectangle(cornerRadius: 14, style: .continuous))
            .padding([.top, .horizontal])
            .shadow(radius: 10)
            
            Form {
                Section(header: Text("Job Description")){
                    
                    WebView(htmlString: job.description, url: nil)
                        .font(.largeTitle)
                        .frame(height: UIScreen.main.bounds.height - 425)
                }
            } // End of form
        } //End of VStack
        .navigationBarItems(trailing:
                                Link(destination: URL(string: job.jobUrl)!) {
                                    Text("Apply")
                                        .fontWeight(.bold)
                                        .frame(width: 100, height: 36, alignment: .center)
                                        .foregroundColor(.white)
                                        .background(
                                            RoundedRectangle(cornerRadius: 16)
                                                .foregroundColor(.accentColor)
                                                .shadow(radius: /*@START_MENU_TOKEN@*/10/*@END_MENU_TOKEN@*/)
                                        )
                                }
        )
    }   // End of body
    
    func dateTimeformat(inputDate: String) -> Text
    {
                /*
                 stringDate comes from the API in different formats after minutes:
                     2020-01-20T15:58:17Z
                     2020-01-19T15:00:11+00:00
                     2020-01-15T18:53:26.2988181Z
                 We only need first 16 characters of stringDate, i.e., 2020-01-20T15:58
                 */
               
                // Take the first 16 characters of stringDate
                let firstPart = inputDate.prefix(16)
               
                // Convert firstPart substring to String
                let cleanedStringDate = String(firstPart)
                
                // Create an instance of DateFormatter
                let dateFormatter = DateFormatter()
               
                // Set date format and locale
                dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm"
                dateFormatter.locale = Locale(identifier: "en_US")
                
                // Convert date String to Date struct
                let dateStruct = dateFormatter.date(from: cleanedStringDate)
                
                // Create a new instance of DateFormatter
                let newDateFormatter = DateFormatter()
                
                newDateFormatter.locale = Locale(identifier: "en_US")
                newDateFormatter.dateStyle = .medium    // Jan 18, 2020
                newDateFormatter.timeStyle = .medium    // at 12:26 PM
                
                // Obtain newly formatted Date String as "Jan 18, 2020 at 12:26 PM"
                let dateWithNewFormat = newDateFormatter.string(from: dateStruct!)
           
                return Text(dateWithNewFormat)
    }
}




