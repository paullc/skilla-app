//
//  WebView.swift
//  Skilla
//
//  Created by CS3714 on 12/5/20.
//  Copyright © 2020 SkillaTeam. All rights reserved.
//

import SwiftUI
import WebKit
 
struct WebView: UIViewRepresentable {
 
    // Input Parameter
    let htmlString: String
    let url: URL?
 
    func makeUIView(context: Context) -> WKWebView  {
        let webView = WKWebView()
        let headerString = "<header><meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no'></header>"
        webView.loadHTMLString(headerString + htmlString, baseURL: url)
            
        return webView
    }
 
    // A WKWebView object displays interactive web content in a web browser within the app
    func updateUIView(_ webView: WKWebView, context: Context) {
        }
    
}
 
