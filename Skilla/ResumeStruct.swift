//
//  ResumeStruct.swift
//  Skilla
//
//  Created by Pau Lleonart Calvo on 12/6/20.
//  Copyright © 2020 SkillaTeam. All rights reserved.
//

import SwiftUI

/*
 Struct stores a user's relevant resume data.
 */
struct ResumeStruct {
    
    var hook: String
    var bio: String
    var education: [String: [String: String]]
    var experience: [String: [String: String]]
    var otherInfo: String
}

