//
//  ContactsData.swift
//  Skilla
//
//  Created by Pau Lleonart Calvo on 12/3/20.
//  Copyright © 2020 SkillaTeam. All rights reserved.
//

import SwiftUI

var contactStructDictionary = [String: Contact]()
var userResumeInformation = ResumeStruct(hook: "", bio: "", education: [String: [String: String]](), experience: [String: [String: String]](), otherInfo: "")
var userContact = Contact(id: "", name: "", age: "", email: "", phone: "", country: "", state: "", city: "", resume: userResumeInformation, profilePic: Image(systemName: "person.circle"))

/*
 This function loads all of the User's contact data from the cloud and continually updates it as changes occur in the database
 */
func loadUserContacts(userData: UserData) {
    
    let userIdentifier = UserDefaults.standard.string(forKey: "userID")!
    
    database.child("Users").child(userIdentifier).observeSingleEvent(of: .value, with: { userSnapshot in

        database.child("Users").child(userIdentifier).child("Contacts").observe(.value, with: { snapshot in
            guard snapshot.hasChild("Identifiers"), let identifiersDictionary = snapshot.value as? [String: Any] else {
                contactStructDictionary = [String: Contact]()
                userData.userContactsDictionary = [String: Contact]()
                print("WARNING: DICTIONARY MAY BE EMPTY")
                return
            }
            
            guard let contactsDictionary = identifiersDictionary["Identifiers"] as? [String : Any] else {
                print("WARNING: COULD NOT READ AS DICTIONARY")
                return

            }
            
            if (contactsDictionary.isEmpty) {
                return
            }
            contactStructDictionary = [String: Contact]()
            parseContactsFromDatabase(contactsData: contactsDictionary, userData: userData)
            
        })
    })
}

/*
 This function refreshes all of the user's contacts manually. This is useful to update images and actual contact profile content since that is not part of the loadContact's observed values.
 */
func refreshUserContacts(userData: UserData) {
    
    let userIdentifier = UserDefaults.standard.string(forKey: "userID")!
    
    database.child("Users").child(userIdentifier).observeSingleEvent(of: .value, with: { userSnapshot in
        guard userSnapshot.hasChild("Contacts") else {
            print("\nWARNING: USER DOES NOT YET HAVE CONTACTS\n")
            return
        }

        database.child("Users").child(userIdentifier).child("Contacts").child("Identifiers").observeSingleEvent(of: .value, with: { snapshot in
            guard let contactsDictionary = snapshot.value as? [String: Any] else {
                print("WARNING: COULD NOT READ AS DICTIONARY")
                return
            }
            
            contactStructDictionary = [String: Contact]()
            parseContactsFromDatabase(contactsData: contactsDictionary, userData: userData)
            
        })
    })
}

/*
 Helper function which takes a dictionary of contact identifiers and searches for them in the database. It wil then load their content locally.
 */
func parseContactsFromDatabase(contactsData: [String: Any], userData: UserData) {

    let requestGroup = DispatchGroup()
    
    for (key, _) in contactsData {
        requestGroup.enter()
        
        guard let contactID = contactsData[key] as? String else {
            return
        }
        
        database.child("Users").child(contactID).observeSingleEvent(of: .value, with: { snapshot in
            guard let contactInfo = snapshot.value as? [String: Any] else {
                return
            }
            
            guard let accountInfo = contactInfo["AccountInfo"] as? [String: Any],
                  let resumeInfo = contactInfo["Resume"] as? [String : Any] else {
                return
            }
            
            let newResume = ResumeStruct(
                hook: (resumeInfo["hook"] as? String ?? "Unavailable").isEmpty ? "Unavailable":
                    resumeInfo["hook"] as? String ?? "Unavailable",
                bio: (resumeInfo["bio"] as? String ?? "Unavailable").isEmpty ? "Unavailable" :
                    resumeInfo["bio"] as? String ?? "Unavailable",
                
                education: (resumeInfo["education"] as? [String: [String: String]] ?? ["None" : ["None": "None"]]).isEmpty ? ["None" : ["None": "None"]] :
                    resumeInfo["education"] as? [String: [String: String]] ?? ["None" : ["None": "None"]],
                
                experience: (resumeInfo["experience"] as? [String: [String: String]] ?? ["None" : ["None" : "None"]]).isEmpty ? ["None" : ["None": "None"]] :
                    resumeInfo["experience"] as? [String: [String: String]] ?? ["None" : ["None": "None"]],
                
                otherInfo: (resumeInfo["otherInfo"] as? String ?? "Unavailable").isEmpty ? "Unavailable" :
                    resumeInfo["otherInfo"] as? String ?? "Unavailable")
            
            var newContact = Contact(
                id: contactID,
                name: (accountInfo["name"] as? String ?? "User").isEmpty ? "User" :
                    accountInfo["name"] as? String ?? "User",
                age: (accountInfo["age"] as? String ?? "Unavailable").isEmpty ? "Unavailable" :
                    accountInfo["age"] as? String ?? "Unavailable",
                email: (accountInfo["email"] as? String ?? "Unavailable").isEmpty ? "Unavailable" : accountInfo["email"] as? String ?? "Unavailable",
                phone: (accountInfo["phone"] as? String ?? "Unavailable").isEmpty ? "Unavailable" : accountInfo["phone"] as? String ?? "Unavailable",
                country: (accountInfo["country"] as? String ?? "Unavailable").isEmpty ? "Unavailable" : accountInfo["country"] as? String ?? "Unavailable",
                state: (accountInfo["state"] as? String ?? "Unavailable").isEmpty ? "Unavailable" : accountInfo["state"] as? String ?? "Unavailable",
                city: (accountInfo["city"] as? String ?? "Unavailable").isEmpty ? "Unavailable" :
                    accountInfo["city"] as? String ?? "Unavailable",
                resume: newResume,
                profilePic: Image(systemName: "person.circle"))
            
            requestGroup.enter()
            storage.child("images/\(contactID).jpg").getData(maxSize: 5 * 1024 * 1024) { (data, error) in
                guard error == nil else {
                    contactStructDictionary[contactID] = newContact
                    
                    requestGroup.leave()
                    print("WARNING: CONTACT IMAGE NOT FOUND")
                    return
                }
                
                newContact.profilePic = Image(uiImage: UIImage(data: data!)!)
                contactStructDictionary[contactID] = newContact
                
                print("CONTACT IMAGE WAS FOUND")
                requestGroup.leave()
            }
            
            requestGroup.leave()
        })
    } // end of for loop
    
    requestGroup.notify(queue: .main) {
        userData.userContactsDictionary = contactStructDictionary
    }
    
}

/*
 Publishes the user's most recent profile information to the firebase database.
 */
func publishUserInfoToDatabase(userInfo: Contact, userData: UserData) {
    
    let userID = UserDefaults.standard.string(forKey: "userID")!
    var contactIDDictionary = [String: Any]()
    
    /*
     Get the contact identifiers of our user's contacts.
     */
    for (key, contact) in userData.userContactsDictionary {
        contactIDDictionary[key] = contact.id
    }
    
    /*
     Here we choose whether to publish a "filler" contact variable. This is useful if the user was just initialized and has no contacts.
     */
    let entryNoContacts: [String: Any] = [
        "Contacts" : [
            "Identifiers" : ""
            ],
        "AccountInfo" : [
            "name" : userInfo.name,
            "age" : userInfo.age,
            "email" : userInfo.email,
            "phone" : userInfo.phone,
            "country" : userInfo.country,
            "state" : userInfo.state,
            "city" : userInfo.city
        ],
        "Resume" : [
            "bio" : userInfo.resume.bio,
            "education" : userInfo.resume.education,
            "experience" : userInfo.resume.experience,
            "otherInfo" : userInfo.resume.otherInfo,
            "hook" : userInfo.resume.hook
        ]
    ]
    let entryContacts: [String: Any] = [
        "Contacts" : [
            "Identifiers" : contactIDDictionary
            ],
        "AccountInfo" : [
            "name" : userInfo.name,
            "age" : userInfo.age,
            "email" : userInfo.email,
            "phone" : userInfo.phone,
            "country" : userInfo.country,
            "state" : userInfo.state,
            "city" : userInfo.city
        ],
        "Resume" : [
            "bio" : userInfo.resume.bio,
            "education" : userInfo.resume.education,
            "experience" : userInfo.resume.experience,
            "otherInfo" : userInfo.resume.otherInfo,
            "hook" : userInfo.resume.hook
        ]
    ]
    
    /*
     Update the user's database contents
     */
    database.child("Users").child(userID).setValue(contactIDDictionary.isEmpty ? entryNoContacts : entryContacts)
    /*
     Change the user's name in the database identifiers if necessary.
     */
    database.child("Identifiers").child(userID).setValue(userInfo.name)
    
    print("\nUSER \(userID) WAS PUBLISHED TO DATABASE\n")
}

/*
 Loads the user's profile information from the database. It will be placed in a local Contact and Resume struct for easy packaging.
 */
func loadUserInfoFromDatabase(userData: UserData) {
    
    let userID = UserDefaults.standard.string(forKey: "userID")!
    let requestGroup = DispatchGroup()
    var foundProfileImage = Image(systemName: "person.circle")
    
    requestGroup.enter()
    database.child("Users").child(userID).observeSingleEvent(of: .value, with: { snapshot in
        guard let userResults = snapshot.value as? [String: Any] else {
            return
        }
        
        guard let accountInfo = userResults["AccountInfo"] as? [String: Any],
              let resumeInfo = userResults["Resume"] as? [String : Any] else {
            return
        }
        
        let newResume = ResumeStruct(
            hook: (resumeInfo["hook"] as? String ?? "Unavailable").isEmpty ? "Unavailable":
                resumeInfo["hook"] as? String ?? "Unavailable",
            bio: (resumeInfo["bio"] as? String ?? "Unavailable").isEmpty ? "Unavailable" :
                resumeInfo["bio"] as? String ?? "Unavailable",
            
            education: (resumeInfo["education"] as? [String: [String: String]] ?? ["None" : ["None": "None"]]).isEmpty ? ["None" : ["None": "None"]] :
                resumeInfo["education"] as? [String: [String: String]] ?? ["None" : ["None": "None"]],
            
            experience: (resumeInfo["experience"] as? [String: [String: String]] ?? ["None" : ["None" : "None"]]).isEmpty ? ["None" : ["None": "None"]] :
                resumeInfo["experience"] as? [String: [String: String]] ?? ["None" : ["None": "None"]],
            
            otherInfo: (resumeInfo["otherInfo"] as? String ?? "Unavailable").isEmpty ? "Unavailable" :
                resumeInfo["otherInfo"] as? String ?? "Unavailable")
        
        let newUserContact = Contact(
            id: userID,
            name: (accountInfo["name"] as? String ?? "User").isEmpty ? "User" :
                accountInfo["name"] as? String ?? "User",
            age: (accountInfo["age"] as? String ?? "Unavailable").isEmpty ? "Unavailable" :
                accountInfo["age"] as? String ?? "Unavailable",
            email: (accountInfo["email"] as? String ?? "Unavailable").isEmpty ? "Unavailable" : accountInfo["email"] as? String ?? "Unavailable",
            phone: (accountInfo["phone"] as? String ?? "Unavailable").isEmpty ? "Unavailable" : accountInfo["phone"] as? String ?? "Unavailable",
            country: (accountInfo["country"] as? String ?? "Unavailable").isEmpty ? "Unavailable" : accountInfo["country"] as? String ?? "Unavailable",
            state: (accountInfo["state"] as? String ?? "Unavailable").isEmpty ? "Unavailable" : accountInfo["state"] as? String ?? "Unavailable",
            city: (accountInfo["city"] as? String ?? "Unavailable").isEmpty ? "Unavailable" :
                accountInfo["city"] as? String ?? "Unavailable",
            resume: newResume,
            profilePic: foundProfileImage)
        
        userContact = newUserContact
        
        requestGroup.enter()
        storage.child("images/\(userID).jpg").getData(maxSize: 5 * 1024 * 1024) { (data, error) in
            guard error == nil else {
                requestGroup.leave()
                print("WARNING: USER IMAGE NOT FOUND")
                return
            }
            
            foundProfileImage = Image(uiImage: UIImage(data: data!)!)

            print("USER IMAGE WAS FOUND")
            requestGroup.leave()
        }
        
        requestGroup.leave()
    })
    
    requestGroup.notify(queue: .main) {
        userContact.profilePic = foundProfileImage
        userData.userContactInformation = userContact
        print("\nNAME: \(userData.userContactInformation.name) WAS LOADED\n")

    }
}

/*
 Deletes a contact from the user's contact list.
 */
public func deleteContact(contactID: String) {
    let userID = UserDefaults.standard.string(forKey: "userID")!
    
    database.child("Users").child(userID).child("Contacts").child("Identifiers").child(contactID).setValue(nil)
    
}
