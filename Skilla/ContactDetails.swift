//
//  ContactDetails.swift
//  Skilla
//
//  Created by Pau Lleonart Calvo on 12/7/20.
//  Copyright © 2020 SkillaTeam. All rights reserved.
//

import SwiftUI

struct ContactDetails: View {
    
    @Environment(\.presentationMode) var presentationMode
    
    init(contact: Contact) {
        UITableViewCell.appearance().backgroundColor = UIColor(Color.red)
        self.contact = contact
    }
    
    let contact: Contact
    @State private var showInfoAlert = false
    @State private var showContactDeleteAlert = false
    var body: some View {
        
        
        ZStack {
        
            /*
             Create linear gradient background
             */
            LinearGradient(gradient: Gradient(colors:
                                                [.red, .purple, .pink]),
                                   startPoint: .top,
                                   endPoint: .bottomTrailing).edgesIgnoringSafeArea(.all)
            
            VStack(alignment: .center, spacing: 20) {
                
                if( contact.profilePic == Image(systemName: "person.circle")) {
                    NavigationLink(destination: QRCodeView(contact: contact)) {
                        contact.profilePic
                            .resizable()
                            .imageScale(.large)
                            .aspectRatio(contentMode: .fit)
                            .foregroundColor(.white)
                            .frame(width: 120)
                            .shadow(radius: 15)
                            .padding(.top, 40)
                    }
                    
                }
                else {
                    NavigationLink(destination: QRCodeView(contact: contact)) {
                        contact.profilePic
                            .resizable()
                            .imageScale(.large)
                            .aspectRatio(contentMode: .fit)
                            .frame(width: 120)
                            .cornerRadius(10)
                            .overlay(RoundedRectangle(cornerRadius: 15)
                                        .stroke(Color.white, lineWidth: 4))
                            .shadow(radius: 15)
                            .padding(.top, 40)
                    }
                    
                }
                
                Text(contact.resume.hook.isEmpty || contact.resume.hook == "Unavailable" ? "A Skilla User" : contact.resume.hook)
                    .foregroundColor(.white)
                    .font(Font.title3.weight(.bold))
                    .shadow(radius: 15)
        
                Form {
                    Group {
                        Section(header: Text("Biography")) {
                            Text(contact.resume.bio)
                                .multilineTextAlignment(.leading)
                        }
                        
                        Section(header: Text("Education")) {
                            let educationKeys = Array(contact.resume.education.keys)

                            ForEach(educationKeys, id: \.self) { key in
                                Text("\(contact.resume.education[key]!["degree"] ?? "None"), \(contact.resume.education[key]!["institution"] ?? "None")")
                                    .multilineTextAlignment(.leading)
                            }
                        }
                        
                        Section(header: Text("Experience")) {
                            let experienceKeys = Array(contact.resume.experience.keys)

                            ForEach(experienceKeys, id: \.self) { key in
                                Text("\(contact.resume.experience[key]!["position"] ?? "None"), \(contact.resume.experience[key]!["organization"] ?? "None")")
                                    .multilineTextAlignment(.leading)
                            }
                        }
                        
                        Section(header: Text("Other Information")) {
                            Text(contact.resume.otherInfo)
                                .multilineTextAlignment(.leading)
                        }
                        
                        Section(header: Text("Phone Number")) {
                            Text(contact.phone == "Unavailable" ? "Phone Unavailable" : contact.phone)
                                .multilineTextAlignment(.leading)
                        }
                    }.alert(isPresented: self.$showContactDeleteAlert, content: { self.contactDeletedAlert })
                    
                    Group {
                        Section(header: Text("Location")) {
                            let city = contact.city == "Unavailable" ? "" : contact.city
                            let state = contact.state == "Unavailable" ? "" : ", \(contact.state)"
                            let country = contact.country == "Unavailable" ? "" : ", \(contact.country)"
                            
                            if (city.isEmpty && state.isEmpty && country.isEmpty) {
                                Text("Location Unavailable")
                            }
                            else {
                                Text("\(city)\(state)\(country)")
                                    .multilineTextAlignment(.leading)
                            }
                            
                        }
                        
                        Section(header: Text("Email")) {
                            if (contact.email == "Unavailable" || contact.name == "Unavailable") {
                                Text("Email Unavailable")
                            }
                            else {
                                Link(destination: URL(string: "mailto:\(contact.email)")!) {
                                    HStack {
                                        Image(systemName: "envelope")
                                            .foregroundColor(.blue)
                                        Text("Send \(contact.name.components(separatedBy: " ").first ?? "User") an Email")
                                            .multilineTextAlignment(.leading)
                                    }
                                }
                            }
                        }
                        
                        Section (header: Text("Delete Contact")) {
                            Button(action: {
                                self.showContactDeleteAlert = true
                                }) {
                                HStack {
                                    Image(systemName: "trash.circle")
                                    .foregroundColor(.blue)
                                    .font(.title3)
                                    Text("Delete")
                                }
                                
                                
                            }
                        }
                        
                        Section (header: Text("Information")) {
                            Button(action: {
                                self.showInfoAlert = true
                                }) {
                                HStack {
                                    Image(systemName: "info.circle")
                                    .foregroundColor(.blue)
                                    .font(.title3)
                                    Text("Show Helpful Information")
                                }
                                
                            }
                        }
                    }.alert(isPresented: self.$showInfoAlert, content: { self.infoAlert })


                    
                
                }// end of Form
                
            }// end of vstack
            .font(.system(size: 14))
            .navigationBarTitle(Text(contact.name.isEmpty ? "User" : "\(contact.name)"), displayMode: .inline)
            
        }// end of zstack
        
        
        
    }// end of body
    
    
    /*
     Show alert with helpful informaiton
     */
    var infoAlert: Alert {
        Alert(title: Text("Skilla Code"),
              message: Text("Tap the profile picture to view this user's Skilla code"),
              dismissButton: .default(Text("OK")) )
    }
    
    /*
     Show alert so that user can select whether or not to delete their contact
     */
    var contactDeletedAlert: Alert {
        Alert(title: Text("Delete Contact?"),
              message: Text("This contact will be deleted until you rescan their code"),
              primaryButton: .default(Text("OK")) {
                self.presentationMode.wrappedValue.dismiss()
                deleteContact(contactID: contact.id)
              },
              secondaryButton: .default(Text("Cancel")) {
                print("Cancelled operation")
              })

    }
}

