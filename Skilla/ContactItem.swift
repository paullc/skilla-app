//
//  ContactItem.swift
//  Skilla
//
//  Created by Pau Lleonart Calvo on 12/6/20.
//  Copyright © 2020 SkillaTeam. All rights reserved.
//

import SwiftUI

struct ContactItem: View {
    let contact: Contact
    
    var body: some View {
        HStack {
            if( contact.profilePic == Image(systemName: "person.circle")) {
                Image("ImageUnavailable")
                    .resizable()
                    .cornerRadius(15)
                    .overlay(RoundedRectangle(cornerRadius: 15)
                                .stroke(LinearGradient(gradient: Gradient(colors:
                                                                            [.red, .purple]),
                                                       startPoint: .top,
                                                       endPoint: .bottomTrailing), lineWidth: 3))
                    .aspectRatio(contentMode: .fit)
                    .frame(width: 65.0)
                    .padding(5)
            }
            else {
                contact.profilePic
                    .resizable()
                    .cornerRadius(15)
                    .overlay(RoundedRectangle(cornerRadius: 15)
                                .stroke(LinearGradient(gradient: Gradient(colors:
                                                                            [.red, .purple]),
                                                       startPoint: .top,
                                                       endPoint: .bottomTrailing), lineWidth: 3))
                    .aspectRatio(contentMode: .fit)
                    .frame(width: 65.0)
                    .padding(5)
            }
            
            VStack(alignment: .leading) {
                Text(contact.name)
                Text(contact.resume.hook)
                    .foregroundColor(Color(UIColor.darkGray))
            }
        }
    }
}

