//
//  ProfilePictureData.swift
//  Skilla
//
//  Created by Pau Lleonart Calvo on 12/6/20.
//  Copyright © 2020 SkillaTeam. All rights reserved.
//

import SwiftUI

public func uploadImageToCloud(imageData: Data) {
    
    storage.child("images/\(UserDefaults.standard.value(forKey: "userID") as! String).jpg").putData(imageData, metadata: nil, completion: { _, error in
        guard error == nil else {
            print("IMAGE UPLOAD FAILED")
            return
        }
        
        print("IMAGE UPLOAD SUCCEEDED")
        
    })
}


