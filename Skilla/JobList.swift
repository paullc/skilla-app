//
//  JobList.swift
//  Skilla
//
//  Created by Pau Lleonart Calvo on 12/3/20.
//  Copyright © 2020 SkillaTeam. All rights reserved.
//

import SwiftUI

struct JobList: View {
    
    @EnvironmentObject var userData: UserData
    
    // Alert: Part 1 of 4
    @State private var showSourceReferenceAlert = false
    
    
    var body: some View {
        NavigationView {
            List {
                // JobStructList is a global array of Job structs given in TravelGuideData.swift
                ForEach(userData.listOfJobs) { aJob in
                    NavigationLink(destination: JobDetails(job: aJob)) {
                        JobItem(job: aJob)
                    }
                }
            }   // End of List
            .navigationBarTitle(Text("Jobs in \(jobCategories[UserDefaults.standard.integer(forKey: "Index")])"), displayMode: .inline)
            .navigationBarItems(trailing:
                Button(action: {
                    // Alert: Part 2 of 4
                    self.showSourceReferenceAlert = true
                }) {
                    Image(systemName: "info.circle")
                        .imageScale(.small)
                        .font(Font.title.weight(.light))
                }
            )
            // Alert: Part 3 of 4
            .alert(isPresented: $showSourceReferenceAlert, content: { self.sourceReferenceAlert })
           
        }   // End of NavigationView
        .customNavigationViewStyle()      // Given in NavigationStyle.swift
       
    }   // End of body
   
    // Alert: Part 4 of 4
    var sourceReferenceAlert: Alert {
        Alert(title: Text("List Info"),
              message: Text("This list contains the 30 most current results based on category selection in the Profiles tab. Otherwise a default list is displayed."),
              dismissButton: .default(Text("OK")) )
    }
}

struct JobList_Previews: PreviewProvider {
    static var previews: some View {
        JobList()
    }
}
