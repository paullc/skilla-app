//
//  ForgotPassword.swift
//  Skilla
//
//  Created by Pau Lleonart Calvo on 12/3/20.
//  Copyright © 2020 SkillaTeam. All rights reserved.
//

import SwiftUI

struct ForgotPassword: View {
    
    @State private var passwordEntered = ""
    @State private var passwordVerified = ""
    @State private var showUnmatchedPasswordAlert = false
    @State private var showPasswordSetAlert = false
    
    @State private var answerEntered = ""
    @State private var answerMatch = false
    @State private var showPassword = false
    @State private var checkIsPressed = false
    
    let securityQuestion = UserDefaults.standard.string(forKey: "SecurityQuestion")
    let securityAnswer = UserDefaults.standard.string(forKey: "SecurityAnswer")
    
    var body: some View {
            ZStack {
                Color.gray.opacity(0.1).edgesIgnoringSafeArea(.all)
                Form {
                    Section(header: Text("Show / hide entered values")) {
                        Toggle(isOn: $showPassword){
                            Text("Show Entered Values")
                        }
                    }
                    Section(header: Text("Security question")) {
                        Text(securityQuestion!)
                    }
                    Section(header: Text("Enter answer to selected security question")){
                        HStack {
                            if self.showPassword {
                                HStack{
                                    TextField("Enter Answer", text: $answerEntered)
                                        .textFieldStyle(RoundedBorderTextFieldStyle())
                                        .autocapitalization(.none)
                                    Button(action: {
                                        self.answerEntered = ""
                                        self.checkIsPressed = false
                                    }){
                                        Image(systemName: "clear")
                                            .imageScale(.medium)
                                            .font(Font.title.weight(.regular))
                                    }
                                }
                                .frame(width: 300, height: 36)
                                .padding()
                            } else {
                                HStack{
                                    SecureField("Enter Answer", text: $answerEntered)
                                        .textFieldStyle(RoundedBorderTextFieldStyle())
                                    Button(action: {
                                        self.answerEntered = ""
                                    }){
                                        Image(systemName: "clear")
                                            .imageScale(.medium)
                                            .font(Font.title.weight(.regular))
                                    }
                                }
                                .frame(width: 300, height: 36)
                                .padding()
                            }
                        }
                    }
                    Button(action: {
                        checkIsPressed = true
                        if !answerEntered.isEmpty {
                            if answerEntered == securityAnswer {
                                self.answerMatch = true
                            }
                        }
                    }) {
                        Text("Check Answer")
                            .frame(width: 250, height: 36, alignment: .center)
                            .background(
                                RoundedRectangle(cornerRadius: 16)
                                    .strokeBorder(Color.black, lineWidth: 1)
                            )
                    }
                    if checkIsPressed {
                        if answerMatch {
                            if showPassword {
                                Section(header: Text("Enter answer to selected security question")) {
                                    HStack{
                                        Text("Answer:")
                                            .fontWeight(.semibold)
                                        TextField("Enter Answer", text: $answerEntered)
                                            .textFieldStyle(RoundedBorderTextFieldStyle())
                                            .autocapitalization(.none)
                                    }
                                    .frame(width: 300, height: 36, alignment: .leading)
                                    .padding()
                                }
                                
                                HStack{
                                    Text("Password:")
                                        .fontWeight(.semibold)
                                    TextField("Enter Password", text: $passwordEntered)
                                        .textFieldStyle(RoundedBorderTextFieldStyle())
                                        .autocapitalization(.none)
                                    
                                }
                                .frame(width: 300, height: 36, alignment: .leading)
                                .padding()
                                
                                HStack{
                                    Text("Verify:")
                                        .fontWeight(.semibold)
                                        .padding(.trailing, 10)
                                    TextField("Verify Password", text: $passwordVerified)
                                        .textFieldStyle(RoundedBorderTextFieldStyle())
                                        .autocapitalization(.none)
                                }
                                .frame(width: 300, height: 36, alignment: .leading)
                                .padding()
                            } else {
                                Section(header: Text("Enter answer to selected security question")) {
                                    HStack{
                                        Text("Answer:")
                                            .fontWeight(.semibold)
                                        SecureField("Enter Answer", text: $answerEntered)
                                            .textFieldStyle(RoundedBorderTextFieldStyle())
                                    }
                                    .frame(width: 300, height: 36, alignment: .leading)
                                    .padding()
                                }
                                HStack{
                                    Text("Password:")
                                        .fontWeight(.semibold)
                                    SecureField("Enter Password", text: $passwordEntered)
                                        .textFieldStyle(RoundedBorderTextFieldStyle())
                                }
                                .frame(width: 300, height: 36, alignment: .leading)
                                .padding()
                                HStack{
                                    Text("Verify:")
                                        .fontWeight(.semibold)
                                        .padding(.trailing, 10)
                                    SecureField("Verify Password", text: $passwordVerified)
                                        .textFieldStyle(RoundedBorderTextFieldStyle())
                                }
                                .frame(width: 300, height: 36, alignment: .leading)
                                .padding()
                            }
                            Button(action: {
                                if !passwordEntered.isEmpty && !answerEntered.isEmpty{
                                    if passwordEntered == passwordVerified {
                                        /*
                                         UserDefaults provides an interface to the user’s defaults database,
                                         where you store key-value pairs persistently across launches of your app.
                                         */
                                        // Store the password in the user’s defaults database under the key "Password"
                                        UserDefaults.standard.set(self.passwordEntered, forKey: "Password")
                                        UserDefaults.standard.set(self.answerEntered, forKey: "SecurityAnswer")
                                        
                                        self.passwordEntered = ""
                                        self.passwordVerified = ""
                                        self.answerEntered = ""
                                        self.showPasswordSetAlert = true
                                    } else {
                                        self.showUnmatchedPasswordAlert = true
                                    }
                                }
                            }) {
                                Text("Set Password")
                                    .frame(minWidth: 100, maxWidth: 600, alignment: .center)
                            } //End of button
                            .alert(isPresented: $showUnmatchedPasswordAlert, content: { self.unmatchedPasswordAlert })
                        } else {
                            Section(header: Text("Incorrect Anwser")){
                                Text("Answer to the Security Question is incorrect!")
                            }
                        }
                    }
                    
                }   // End of Form
            }   // End of ZStack
            .navigationBarTitle(Text("Password Reset"), displayMode: .inline)
    }
    
    /*
    --------------------------
    MARK: - Password Set Alert
    --------------------------
    */
    var passwordSetAlert: Alert {
    Alert(title: Text("Password Set!"),
    message: Text("Password you entered is set to unlock the app!"),
    dismissButton: .default(Text("OK")) )
    }

    /*
    --------------------------------
    MARK: - Unmatched Password Alert
    --------------------------------
    */
    var unmatchedPasswordAlert: Alert {
    Alert(title: Text("Unmatched Password!"),
    message: Text("Two entries of the password must match!"),
    dismissButton: .default(Text("OK")) )
    }

}

struct ForgotPassword_Previews: PreviewProvider {
    static var previews: some View {
        ForgotPassword()
    }
}
