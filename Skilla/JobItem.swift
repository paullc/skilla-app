//
//  JobItem.swift
//  Skilla
//
//  Created by Pau Lleonart Calvo on 12/3/20.
//  Copyright © 2020 SkillaTeam. All rights reserved.
//

import SwiftUI

struct JobItem: View {
    // Input Parameter
    let job: Job
    
    var body: some View {
        HStack {
            VStack{
                Text("Days Since Post")
                    .font(.caption2)
                    .foregroundColor(.gray)
                daysSincePost(inputDate: job.publicationDate)
                    .foregroundColor(.blue)
                    .font(.system(size: 30))
                    .shadow(radius: 10)
                    .frame(width: 80)
            }
            VStack(alignment: .leading) {
                Text(job.title)
                    .font(.headline)
                Text(job.companyName)
                    .font(.subheadline)
                    .fontWeight(.semibold)
                    .foregroundColor(Color(UIColor.darkGray))
                Text(job.jobType.capitalized)
                    .font(.subheadline)
                    .foregroundColor(Color(UIColor.darkGray))
            }
            // Set font and size for the whole VStack content
            .font(.system(size: 14))
            
        }   // End of HStack
    }
    
    func daysSincePost(inputDate: String) -> Text {
        
        // Take the first 16 characters of stringDate
        let firstPart = inputDate.prefix(16)
        
        // Convert firstPart substring to String
        let cleanedStringDate = String(firstPart)
        
        // Create an instance of DateFormatter
        let dateFormatter = DateFormatter()
        
        // Set date format and locale
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm"
        dateFormatter.locale = Locale(identifier: "en_US")
        
        // Convert date String to Date struct
        let dateStruct = dateFormatter.date(from: cleanedStringDate)
        let currentDate = Date()
        
        let diff = Calendar.current.dateComponents([.day], from: dateStruct!, to: currentDate)
        
        if(String(diff.day!) == "0")
        {
            return Text("Today")
        }
        else{
            return Text(String(diff.day!))
        }
    }
}


