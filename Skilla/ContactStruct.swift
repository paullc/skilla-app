//
//  ContactStruct.swift
//  Skilla
//
//  Created by Pau Lleonart Calvo on 12/3/20.
//  Copyright © 2020 SkillaTeam. All rights reserved.
//

import SwiftUI

/*
 Struct to store all information relevant to any user
 */
struct Contact: Identifiable {
    
    var id: String
    var name: String
    var age: String
    var email: String
    var phone: String
    var country: String
    var state: String
    var city: String
    var resume: ResumeStruct
    var profilePic: Image
}
