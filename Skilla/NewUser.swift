//
//  NewUser.swift
//  Skilla
//
//  Created by CS3714 on 12/3/20.
//  Copyright © 2020 SkillaTeam. All rights reserved.
//

import SwiftUI

struct NewUser: View {
    
    @EnvironmentObject var userData: UserData

    @State private var passwordEntered = ""
    @State private var passwordVerified = ""
    @State private var nameEntered = ""
    @State private var showValues = false
    @State private var showUnmatchedPasswordAlert = false
    @State private var showPasswordSetAlert = false
    
    let colors = [Color.red, Color.purple, Color.orange]
    
    var body: some View {
            ZStack {
                LinearGradient(gradient: Gradient(colors: colors), startPoint: .topLeading, endPoint: .bottomTrailing)
                    .edgesIgnoringSafeArea(.all)
                VStack{
                    Image(systemName: "person.circle")
                        .resizable()
                        .aspectRatio(contentMode: .fit)
                        .frame(minWidth: 85, maxWidth: 85)
                        .padding(.horizontal, 30)
                        .padding(.top, 20)
                        .foregroundColor(.white)
                        .shadow(radius: /*@START_MENU_TOKEN@*/10/*@END_MENU_TOKEN@*/)
                        .padding(.bottom, 10)
                    
                   Form {
                        
                        Toggle(isOn: $showValues){
                            Text("Show Entered Values")
                        }
                        
                        if self.showValues {
                            HStack{
                                Text("Password:")
                                    .fontWeight(.semibold)
                                TextField("Enter Password", text: $passwordEntered)
                                    .textFieldStyle(RoundedBorderTextFieldStyle())
                                    .autocapitalization(.none)
                                
                            }
                            .frame(width: 300, height: 36, alignment: .leading)
                            .padding()
                            
                            HStack{
                                Text("Verify:")
                                    .fontWeight(.semibold)
                                    .padding(.trailing, 10)
                                TextField("Verify Password", text: $passwordVerified)
                                    .textFieldStyle(RoundedBorderTextFieldStyle())
                                    .autocapitalization(.none)
                            }
                            .frame(width: 300, height: 36, alignment: .leading)
                            .padding()
                            
                            //USER'S NAME ENTERED HERE
                            HStack{
                                Text("Name:")
                                    .fontWeight(.semibold)
                                    .padding(.trailing, 10)
                                TextField("Enter Your Full Name", text: $nameEntered)
                                    .textFieldStyle(RoundedBorderTextFieldStyle())
                                    .autocapitalization(.words)
                            }
                            .frame(width: 300, height: 36, alignment: .leading)
                            .padding()
                        } else {
                            HStack{
                                Text("Password:")
                                    .fontWeight(.semibold)
                                SecureField("Enter Password", text: $passwordEntered)
                                    .textFieldStyle(RoundedBorderTextFieldStyle())
                            }
                            .frame(width: 300, height: 36, alignment: .leading)
                            .padding()
                            HStack{
                                Text("Verify:")
                                    .fontWeight(.semibold)
                                    .padding(.trailing, 10)
                                SecureField("Verify Password", text: $passwordVerified)
                                    .textFieldStyle(RoundedBorderTextFieldStyle())
                            }
                            .frame(width: 300, height: 36, alignment: .leading)
                            .padding()
                            
                            //USER'S NAME ENTERED HERE
                            HStack{
                                Text("Name:")
                                    .fontWeight(.semibold)
                                    .padding(.trailing, 10)
                                TextField("Enter Your Full Name", text: $nameEntered)
                                    .textFieldStyle(RoundedBorderTextFieldStyle())
                                    .autocapitalization(.words)
                            }
                            .frame(width: 300, height: 36, alignment: .leading)
                            .padding()
                            
                        }
                        
                    
                        Button(action: {
                            if !passwordEntered.isEmpty {
                                if passwordEntered == passwordVerified {
                                    /*
                                     UserDefaults provides an interface to the user’s defaults database,
                                     where you store key-value pairs persistently across launches of your app.
                                     */
                                    // Store the password in the user’s defaults database under the key "Password"
                                    UserDefaults.standard.set(self.passwordEntered, forKey: "Password")
                                    
                                    /*
                                     The user has successfully registered themselves, so generate a UUID string and place is in the UserDefaults so it can by used by the whole app
                                     */
                                    UserDefaults.standard.setValue(UUID().uuidString, forKey: "userID")
                                    
                                    userData.userContactInformation.name = nameEntered
                                                
                                    /*
                                     User is not registed. We can take the initalizer struct and push it to the database to set up our keys.
                                     */
                                    publishUserInfoToDatabase(userInfo: userData.userContactInformation, userData: userData)

                                    self.passwordEntered = ""
                                    self.passwordVerified = ""
                                    
                                    self.showPasswordSetAlert = true
                                } else {
                                    self.showUnmatchedPasswordAlert = true
                                }
                            }
                        }) {
                            Text("Finish Account Setup")
                                .frame(minWidth: 100, maxWidth: 600, alignment: .center)
                        } //End of button
                        
                        .alert(isPresented: $showPasswordSetAlert, content: { self.passwordSetAlert })
                    } // End of Form\
                    .alert(isPresented: $showUnmatchedPasswordAlert, content: { self.unmatchedPasswordAlert })
                    .navigationBarTitle(Text("Account Setup"), displayMode: .inline)
                } //End of VStack
            }  // End of ZStack
    } // End of body
    /*
     --------------------------
     MARK: - Password Set Alert
     --------------------------
     */
    var passwordSetAlert: Alert {
        Alert(title: Text("Password Set!"),
              message: Text("Password you entered is set to unlock the app!"),
              dismissButton: .default(Text("OK")) )
    }
    
    /*
     --------------------------------
     MARK: - Unmatched Password Alert
     --------------------------------
     */
    var unmatchedPasswordAlert: Alert {
        Alert(title: Text("Unmatched Password!"),
              message: Text("Two entries of the password must match!"),
              dismissButton: .default(Text("OK")) )
    }
}

struct NewUser_Previews: PreviewProvider {
    static var previews: some View {
        NewUser()
    }
}
