//
//  ContactsList.swift
//  Skilla
//
//  Created by Pau Lleonart Calvo on 12/3/20.
//  Copyright © 2020 SkillaTeam. All rights reserved.
//

import SwiftUI

struct ContactsList: View {
    
    @EnvironmentObject var userData: UserData
    
    l

    let alphabet = ["No Name", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U" ,"V", "W", "X", "Y", "Z"]
        
    var body: some View {
        NavigationView {
            List {
                SearchBar(searchItem: $searchItem, placeholder: "Search My Contacts")
                
                let valuesList = Array(userData.userContactsDictionary.values)
                
                /*
                 Filter items by search bar
                 */
                let showableItems = valuesList.filter { contact in
                    return searchItem.isEmpty || contact.name.localizedStandardContains(searchItem)
                }
                
                ForEach(alphabet, id: \.self) {aLetter in
                    /*
                     Further filter items by first letter of name
                     */
                    let listWithLetters = showableItems.filter { contact in
                        if (aLetter == "No Name" && contact.name.isEmpty) {
                            return true
                        }
                        return contact.name.prefix(1).uppercased() == aLetter
                           
                    }
                    
                    
                    if !listWithLetters.isEmpty {
                        Section(header: Text(aLetter).font(Font.title3.weight(.bold))) {
                            ForEach(listWithLetters){ aContact in
                                NavigationLink(destination: ContactDetails(contact: aContact)) {
                                    ContactItem(contact: aContact)
                                }
                            }

                        }// end of section
                    }
                    
                }
                
                
            } // End of List
            .navigationViewStyle(StackNavigationViewStyle())
            .navigationBarItems(trailing:
                                    Button(action: {
                                        refreshUserContacts(userData: userData)
                                    }) {
                                        Image(systemName: "arrow.clockwise.circle")
                                            .foregroundColor(.blue)
                                            .font(.title3)
                                    })
            .navigationBarTitle(Text("My Contacts"))
            .listStyle(InsetGroupedListStyle())
            .environment(\.horizontalSizeClass, .regular)
    //        .font(.system(size: 14))
        }//end of navigation view

    }
    
}
