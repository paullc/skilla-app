//
//  Setting.swift
//  Skilla
//
//  Created by CS3714 on 12/5/20.
//  Copyright © 2020 SkillaTeam. All rights reserved.
//

import SwiftUI

struct Setting: View {
    
    @EnvironmentObject var userData: UserData

    // Enable this View to be dismissed to go back when the Save button is tapped
    @Environment(\.presentationMode) var presentationMode
    
    // ❎ CoreData managedObjectContext reference
    @Environment(\.managedObjectContext) var managedObjectContext
    
    // Profile Photo
    @State private var showImagePicker = false
    @State private var photoImageData: Data? = nil
    @State private var photoTakeOrPickIndex = 1     // Pick from Photo Library
    
    @State private var passwordEntered = ""
    @State private var passwordVerified = ""
    @State private var answerEntered = ""
    @State private var showValues = false
    @State private var showUnmatchedPasswordAlert = false
    @State private var showPasswordSetAlert = false
    @State private var showInvalidEntryAlert = false
    @State private var selectedIndex = 0
    
    let securityQuestionsList = ["In what city or town did your mother and father meet?", "In what city were you born?", "What did you want to be when you grew up?", "What do you remember most from your childhood?", "What is the name of the boy or girl you first kissed?", "What is the name of the first school you attended?", "What is the anme of your favorite childhood friend?", "What is the name of your first pet?", "What is your mother's maiden name?", "What was your favorite place to visit as a child?"]
    
    var photoTakeOrPickChoices = ["Camera", "Photo Library"]
    
    let securityAnswer = UserDefaults.standard.string(forKey: "SecurityAnswer")
    
    var body: some View {
        ZStack {
            Color.gray.opacity(0.1).edgesIgnoringSafeArea(.all)
            VStack{
                
                Form {
                    Section(header: Text("Add Profile Photo")) {
                        VStack {
                            Picker("Take or Pick Photo", selection: $photoTakeOrPickIndex) {
                                ForEach(0 ..< photoTakeOrPickChoices.count, id: \.self) {
                                    Text(self.photoTakeOrPickChoices[$0])
                                }
                            }
                            .pickerStyle(SegmentedPickerStyle())
                            .padding()
                            Divider()
                            Button(action: {
                                self.showImagePicker = true
                            }) {
                                Text("Get Profile Photo")
                                    .padding()
                            }
                        }   // End of VStack
                    } // End of section
                    .alert(isPresented: $showInvalidEntryAlert, content: { self.invalidEntryAlert })
                    
                    Toggle(isOn: $showValues){
                        Text("Show Entered Values")
                    }
                    
                    Section(header: Text("Select a security question")) {
                        
                        // Picker: Part 3 of 3
                        Picker("Selected:", selection: $selectedIndex) {
                            ForEach(0 ..< securityQuestionsList.count, id: \.self) {
                                Text(self.securityQuestionsList[$0])
                                    .font(.footnote)
                            }
                        }
                    }
                    .alert(isPresented: $showUnmatchedPasswordAlert, content: { self.unmatchedPasswordAlert })
                    
                    if self.showValues {
                        Section(header: Text("Enter answer to selected security question")) {
                            HStack{
                                Text("Answer:")
                                    .fontWeight(.semibold)
                                TextField("Enter Answer", text: $answerEntered)
                                    .textFieldStyle(RoundedBorderTextFieldStyle())
                                    .autocapitalization(.none)
                            }
                            .frame(width: 300, height: 36, alignment: .leading)
                            .padding()
                        }
                        
                        HStack{
                            Text("Password:")
                                .fontWeight(.semibold)
                            TextField("Enter Password", text: $passwordEntered)
                                .textFieldStyle(RoundedBorderTextFieldStyle())
                                .autocapitalization(.none)
                            
                        }
                        .frame(width: 300, height: 36, alignment: .leading)
                        .padding()
                        
                        HStack{
                            Text("Verify:")
                                .fontWeight(.semibold)
                                .padding(.trailing, 10)
                            TextField("Verify Password", text: $passwordVerified)
                                .textFieldStyle(RoundedBorderTextFieldStyle())
                                .autocapitalization(.none)
                        }
                        .frame(width: 300, height: 36, alignment: .leading)
                        .padding()
                    } else {
                        Section(header: Text("Enter answer to selected security question")) {
                            HStack{
                                Text("Answer:")
                                    .fontWeight(.semibold)
                                SecureField("Enter Answer", text: $answerEntered)
                                    .textFieldStyle(RoundedBorderTextFieldStyle())
                            }
                            .frame(width: 300, height: 36, alignment: .leading)
                            .padding()
                        }
                        HStack{
                            Text("Password:")
                                .fontWeight(.semibold)
                            SecureField("Enter Password", text: $passwordEntered)
                                .textFieldStyle(RoundedBorderTextFieldStyle())
                        }
                        .frame(width: 300, height: 36, alignment: .leading)
                        .padding()
                        HStack{
                            Text("Verify:")
                                .fontWeight(.semibold)
                                .padding(.trailing, 10)
                            SecureField("Verify Password", text: $passwordVerified)
                                .textFieldStyle(RoundedBorderTextFieldStyle())
                        }
                        .frame(width: 300, height: 36, alignment: .leading)
                        .padding()
                    }
                    
                    Button(action: {
                        if !passwordEntered.isEmpty && !answerEntered.isEmpty {
                            
                            if passwordEntered == passwordVerified {
                                /*
                                 UserDefaults provides an interface to the user’s defaults database,
                                 where you store key-value pairs persistently across launches of your app.
                                 */
                                // Store the password in the user’s defaults database under the key "Password"
                                UserDefaults.standard.set(self.passwordEntered, forKey: "Password")
                                UserDefaults.standard.set(self.answerEntered, forKey: "SecurityAnswer")
                                UserDefaults.standard.set(securityQuestionsList[selectedIndex], forKey: "SecurityQuestion")
                                
                                self.passwordEntered = ""
                                self.passwordVerified = ""
                                self.answerEntered = ""
                                self.showPasswordSetAlert = true
                            } else {
                                self.showUnmatchedPasswordAlert = true
                            }
                            
                        } else {
                            self.showInvalidEntryAlert = true
                        }
                        
                    }) {
                        Text("Set Password")
                            .frame(minWidth: 100, maxWidth: 600, alignment: .center)
                    }
                } //End of Form
                .alert(isPresented: $showPasswordSetAlert, content: { self.passwordSetAlert })
            } // End of VStack
            .navigationBarTitle(Text("Settings"), displayMode: .inline)
            .sheet(isPresented: self.$showImagePicker) {
                /*
                 🔴 We pass $showImagePicker and $photoImageData with $ sign into PhotoCaptureView
                 so that PhotoCaptureView can change them. The @Binding keywork in PhotoCaptureView
                 indicates that the input parameter is passed by reference and is changeable (mutable).
                 */
                PhotoCaptureView(showImagePicker: self.$showImagePicker,
                                 photoImageData: Binding(
                                    get: { self.photoImageData },
                                    set: { (newImageData) in
                                        self.photoImageData = newImageData
                                        if newImageData != nil {
                                            uploadImageToCloud(imageData: newImageData!)
                                            userData.userContactInformation.profilePic = getImageFromBinaryData(binaryData: newImageData, defaultSystemName: "person.circle")
                                        }
                                    }
                                 ),
                                 cameraOrLibrary: self.photoTakeOrPickChoices[self.photoTakeOrPickIndex])
            } //End of VStack
        }  // End of ZStack
    } // End of body
    /*
     --------------------------
     MARK: - Password Set Alert
     --------------------------
     */
    var passwordSetAlert: Alert {
        Alert(title: Text("Password Set!"),
              message: Text("Password you entered is set to unlock the app!"),
              dismissButton: .default(Text("OK")) )
    }
    
    /*
     --------------------------------
     MARK: - Unmatched Password Alert
     --------------------------------
     */
    var unmatchedPasswordAlert: Alert {
        Alert(title: Text("Unmatched Password!"),
              message: Text("Two entries of the password must match!"),
              dismissButton: .default(Text("OK")) )
    }
    
    /*
     --------------------------------
     MARK: - No Backup Alert
     --------------------------------
     */
    var invalidEntryAlert: Alert {
        Alert(title: Text("Enter the required inputs!"),
              message: Text("Please enter a password and an anwser to the security question!"),
              dismissButton: .default(Text("OK")) )
    }
    
    /*
     Presents the Image Data
     */
    var photoImage: Image {
        
        if let imageData = self.photoImageData {
            // The public function is given in UtilityFunctions.swift
            let imageView = getImageFromBinaryData(binaryData: imageData, defaultSystemName: "person.circle")
            return imageView
        } else {
            return Image("ImageUnavailable")
        }
    }
    
}

struct Setting_Previews: PreviewProvider {
    static var previews: some View {
        Setting()
    }
}

