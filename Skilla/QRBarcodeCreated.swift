////
////  QRBarcodeCreated.swift
////  Skilla
////
////  Created by Pau Lleonart Calvo on 12/3/20.
////  Copyright © 2020 SkillaTeam. All rights reserved.
////
//
//import SwiftUI
//import UIKit
//
//struct QRBarcodeCreated: View {
//
//    // Input Parameters
//    let url: String
//    let apiQuery: String
//
//    var body: some View {
//        ZStack {                // Background
//            Color.gray.opacity(0.1).edgesIgnoringSafeArea(.all)
//
//            // Insert ActivityViewController within the window hierarchy in the background
//            activityViewController
//
//        VStack(spacing: 40) {   // Foreground
//
//            Text(url)
//                .font(.headline)
//
//            // Show the QR barcode image returned from the API
//            qrBarcodeImage(imageUrlString: apiQuery)
//                .resizable()
//                .aspectRatio(contentMode: .fit)
//                .frame(minWidth: 300, maxWidth: 500, alignment: .center)
//                .padding(.horizontal)
//
//            Button(action: {
//                // Display the Share interface to Share uiImageOfQRBarcode
//                activityViewController.shareImage(uiImage: uiImageOfQRBarcode)
//            }) {
//                HStack {
//                    Image(systemName:"square.and.arrow.up")
//                        .imageScale(.medium)
//                        .font(Font.title.weight(.regular))
//                        .foregroundColor(.blue)
//                    Text("Share")
//                        .font(.system(size: 16))
//                }
//            }
//            /*
//             Enable the user to save the created barcode image in the Photos app
//             by adding the following row in Info.plist file:
//                 Privacy - Photo Library Additions Usage Description -->
//                 BarcodeCreator can store the created barcode image in your Photos app upon your permission.
//             */
//        }   // End of VStack
//        }   // End of ZStack
//    }   // End of body
//}
