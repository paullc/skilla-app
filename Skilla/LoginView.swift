//
//  LoginView.swift
//  Skilla
//
//  Created by Pau Lleonart Calvo on 12/3/20.
//  Copyright © 2020 SkillaTeam. All rights reserved.
//

import LocalAuthentication
import AVFoundation
import SwiftUI

struct LoginView: View {
    
    @EnvironmentObject var userData: UserData
    
    @State private var enteredPassword = ""
    @State private var showInvalidPasswordAlert = false
    let colors = [Color.red, Color.purple]
    
    var body: some View {
        NavigationView{
            ZStack {
                LinearGradient(gradient: Gradient(colors: colors), startPoint: .topLeading, endPoint: .bottomTrailing)
                    .edgesIgnoringSafeArea(.all)
                ScrollView(.vertical, showsIndicators: false) {
                    VStack {
                        Image(systemName: "person.circle")
                            .resizable()
                            .aspectRatio(contentMode: .fit)
                            .frame(minWidth: 100, maxWidth: 100)
                            .padding(.horizontal, 30)
                            .padding(.top, 100)
                            .foregroundColor(.white)
                            .shadow(radius: /*@START_MENU_TOKEN@*/10/*@END_MENU_TOKEN@*/)
                        
                        SecureField("Password", text: $enteredPassword)
                            .textFieldStyle(RoundedBorderTextFieldStyle())
                            .frame(width: 300, height: 36)
                            .padding()
                        
                        HStack{
                            Button(action: {
                                /*
                                 UserDefaults provides an interface to the user’s defaults database,
                                 where you store key-value pairs persistently across launches of your app.
                                 */
                                // Retrieve the password from the user’s defaults database under the key "Password"
                                let validPassword = UserDefaults.standard.string(forKey: "Password")
                                var category = UserDefaults.standard.string(forKey: "Category")
                                
                                
                                /*
                                 If the user has not yet set a password, validPassword = nil
                                 In this case, allow the user to login.
                                 */
                                if self.enteredPassword == validPassword {
                                    userData.userAuthenticated = true
                                    self.showInvalidPasswordAlert = false
                                    
                                    if category == "" || category == nil {
                                        category = "Sales"
                                    }                                    
                                    
                                    /*
                                     At this point the user is registered, and has logged in. We make a request to get the user's information, and the information of all of the user's contacts from the database
                                     */
                                    loadUserContacts(userData: userData)
                                    loadUserInfoFromDatabase(userData: userData)
                                    
                                    switch AVCaptureDevice.authorizationStatus(for: .video) {
                                        case .authorized: // The user has previously granted access to the camera.
                                            return
                                        case .notDetermined: // The user has not yet been asked for camera access.
                                            AVCaptureDevice.requestAccess(for: .video) {granted in
                                                print("Camera Access Granted: \(granted)")
                                            }
                                        
                                        case .denied: // The user has previously denied access.
                                            return

                                        case .restricted: // The user can't grant access due to restrictions.
                                            return
                                        @unknown default:
                                            return
                                    }
                                    
                                    
                                    searchFromJobsApi(category: category!)
                                    userData.listOfJobs = jobsList
                                } else {
                                    self.showInvalidPasswordAlert = true
                                }
                                
                            }) {
                                Text("Login")
                                    .font(Font.subheadline.weight(.semibold))
                                    .frame(width: 100, height: 36, alignment: .center)
                                    .foregroundColor(.white)
                                    .background(
                                        RoundedRectangle(cornerRadius: 16)
                                            .foregroundColor(.accentColor)
                                            .shadow(radius: /*@START_MENU_TOKEN@*/10/*@END_MENU_TOKEN@*/)
                                    )
                            }
                            .padding(.leading, UserDefaults.standard.string(forKey: "SecurityAnswer") != nil ? 25 : 0)
                            .padding(.bottom, 10)
                            .alert(isPresented: $showInvalidPasswordAlert, content: { self.invalidPasswordAlert })
                            if UserDefaults.standard.string(forKey: "SecurityAnswer") != nil {
                                NavigationLink(destination: ForgotPassword()){
                                    Text("Forgot Password")
                                        .font(Font.subheadline.weight(.semibold))
                                        .frame(width: 180, height: 36, alignment: .center)
                                        .foregroundColor(.white)
                                        .background(
                                            RoundedRectangle(cornerRadius: 16)
                                                .foregroundColor(.accentColor)
                                                .shadow(radius: /*@START_MENU_TOKEN@*/10/*@END_MENU_TOKEN@*/)
                                        )
                                }.buttonStyle(PlainButtonStyle()) //End of Navigationlink
                                .padding(.trailing, 25)
                                .padding(.bottom,  10)
                            }
                        } //End of HStack
                        
                        if UserDefaults.standard.string(forKey: "Password") == nil {
                            HStack{
                                Text("New User? ")
                                    .foregroundColor(.white)
                                NavigationLink(destination: NewUser()){
                                    Text("Sign up")
                                        .foregroundColor(.white)
                                        .underline()
                                        .fontWeight(.bold)
                                        .shadow(radius: 5)
                                }
                            }
                        }
                        
                    }   // End of VStack
                }   // End of ScrollView
            }   // End of ZStack
            .onAppear(perform: authenticate)
        }
        .navigationViewStyle(StackNavigationViewStyle())
    }   // End of body
    
    /*
     ------------------------------
     MARK: - Invalid Password Alert
     ------------------------------
     */
    var invalidPasswordAlert: Alert {
        Alert(title: Text("Invalid Password!"),
              message: Text("Please enter a valid password to unlock the app!"),
              dismissButton: .default(Text("OK")) )
        
        // Tapping OK resets @State var showInvalidPasswordAlert to false.
    }
    
    func authenticate() {
        let context = LAContext()
        var error: NSError?
        
        // check whether biometric authentication is possible
        if context.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: &error)  && UserDefaults.standard.string(forKey: "Password") != nil{
            // it's possible, so go ahead and use it
            let reason = "We need to unlock your data."
            
            context.evaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, localizedReason: reason) { success, authenticationError in
                // authentication has now completed
                DispatchQueue.main.async {
                    if success {
                        
                        var category = UserDefaults.standard.string(forKey: "Category")
                        
                        if category == "" || category == nil {
                            category = "Sales"
                        }
                       
                        searchFromJobsApi(category: category!)
                        
                        /*
                         At this point the user is registered, and has logged in. We make a request to get the user's information, and the information of all of the user's contacts from the database
                         */
                        loadUserContacts(userData: userData)
                        loadUserInfoFromDatabase(userData: userData)
                        
                        
                        
                        userData.listOfJobs = jobsList
                        userData.userAuthenticated = true
                    } else {
                        // there was a problem
                        return
                    }
                }
            }
        } else {
        }
    }
    
}

struct LoginView_Previews: PreviewProvider {
    static var previews: some View {
        LoginView()
    }
}

