//
//  JobStruct.swift
//  Skilla
//
//  Created by Pau Lleonart Calvo on 12/3/20.
//  Copyright © 2020 SkillaTeam. All rights reserved.
//

import SwiftUI
 
struct Job: Hashable, Codable, Identifiable {
   
    var id: UUID        // Storage Type: String, Use Type (format): UUID
    var jobUrl: String
    var title: String
    var companyName: String
    var jobCategory: String
    var jobType: String
    var publicationDate: String
    var salaryRange: String
    var description: String
}


/*
 {
     "0-legal-notice": "Remotive API Legal Notice",
     "job-count": 1, // Number or jobs matching the query == length of 'jobs' list
     "jobs": [ // The list of all jobs retrieved. Then for each job, you get:
     {
     "id": 123, // Unique Remotive ID
     "url": "https://remotive.io/remote-jobs/product/lead-developer-123", // Job
     listing detail url
     "title": "Lead Developer", // Job title
     "company_name": "Remotive", // Name of the company which is hiring
     "category": "Software Development", // See
     https://remotive.io/api/remote-jobs/categories for existing categories
     "job_type": "full_time", // "full_time" or "contract" here. It's optional an often not filled.
     "publication_date": "2020-02-15T10:23:26", // Publication date and time on
     https://remotive.io
     "candidate_required_location": "Worldwide", // Geographical restriction for the
     remote candidate, if any.
     "salary": "$40,000 - $50,000", // salary description, usually a yearly salary
     range, in USD. It's optional an often not filled.
     "description": "The full HTML job description here", // HTML full description of
     the job listing
     },
     ]
     }
 */
