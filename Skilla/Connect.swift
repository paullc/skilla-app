//
//  Connect.swift
//  Skilla
//
//  Created by Pau Lleonart Calvo on 12/3/20.
//  Copyright © 2020 SkillaTeam. All rights reserved.
//

import SwiftUI
import FirebaseDatabase

struct Connect: View {
    
    @EnvironmentObject var userData: UserData

    @State private var barcode: String = ""
    @State var lightOn: Bool = false
    @State var showResults: Bool = false
    @State var operationStatus: String = ""
    @State var foundProfilePic: Image =  Image(systemName: "person.circle")
    @State var foundName: String =  "User"
    
    var body: some View {
        VStack {
            // Show barcode scanning camera view if no barcode is present
           
            if self.barcode.isEmpty {
                /*
                 Display barcode scanning camera view on the background layer because
                 we will display the results on the foreground layer in the same view.
                 */
                ZStack {
                    /*
                     BarcodeScanner displays the barcode scanning camera view, obtains the barcode
                     value, and stores it into the @State variable 'barcode'. When the @State value
                     changes,the view invalidates its appearance and recomputes this body view.
                    
                     When this body view is recomputed, 'barcode' will not be empty and the
                     else part of the if statement will be executed, which displays barcode
                     processing results on the foreground layer in this same view.
                     */
                    
                    /*
                     Use binding string so we can learn when it has been changed in order to fire the database retrieval function
                     */
                    BarcodeScanner(code: Binding(
                        get: {self.barcode},
                        set: { (newValue) in
                            self.barcode = newValue
                            addNewContact(contactID: newValue)
                        }
                    )).border(
                        LinearGradient(gradient: Gradient(colors:
                                                                    [.red, .purple]),
                                               startPoint: .top,
                                               endPoint: .bottomTrailing), width: 6)
                        
                    // Display the flashlight button view
                    FlashlightButtonView(lightOn: self.$lightOn)
                   
                    /*
                     Display the scan focus region image to guide the user during scanning.
                     The image is constructed in ScanFocusRegion.swift upon app launch.
                     */
//                        scanFocusRegionImage
                }
            }
            /*
             Based on the database results, display the proper situation view
             */
            else if (self.showResults && !self.operationStatus.isEmpty){
                if (self.operationStatus == "contactExists") {
                    self.contactExistsResult
                }
                else if (self.operationStatus == "userAdded") {
                    self.userAddedResult
                }
                else if (self.operationStatus == "userDoesntExist") {
                    self.userDoesntExistResult
                }
                else if (self.operationStatus == "invalidCode") {
                    self.invalidQRCodeResult
                }
            }
        }   // End of VStack
        .navigationBarTitle(Text("Add A New Contact"), displayMode: .inline)
        .onDisappear() {
            self.lightOn = false
                
        }
        
    }
    
    /*
     Adds new contact with UUID contactID to User's realtime database.
     */
    func addNewContact(contactID: String) {
        let forbiddenCharacters = [".", "#", "$", "[", "]"]
        
        if (forbiddenCharacters.contains(where: contactID.contains)) {
            self.operationStatus = "invalidCode"
            self.showResults = true
            return
            
        }
        
        print("SEARCHING FOR ID: \(contactID)")
        
        /*
         To save bandwidth, check if the user is already present locally. If so, we can spare ourselves the data transfer from the database and image storage.
         */
        if userData.userContactsDictionary[contactID] != nil || (UserDefaults.standard.string(forKey: "userID") == contactID) {
            self.operationStatus = "contactExists"
            self.showResults = true
            
            return
        }
        
        /*
         User dispatch groups to make sure certain blocks of code are not executed too soon
         */
        let userID = UserDefaults.standard.string(forKey: "userID")!
        let requestGroup = DispatchGroup()
        
        requestGroup.enter()
        database.child("Identifiers").observeSingleEvent(of: .value) { snapshot in
            guard let identifiers = snapshot.value as? [String: String], snapshot.hasChild(contactID) else {
                print("\nTHERE IS NO CHILD WITH THIS IDENTIFIER\n")
                self.operationStatus = "userDoesntExist"
                self.showResults = true
                return
            }
            
            self.foundName = identifiers[contactID]!
            
            /*
             Update contacts in our database. This will fire the contact list's database listener
             */
            requestGroup.enter()
            database.child("Users").child(userID).child("Contacts").child("Identifiers").child(contactID).setValue(contactID) {
                (error:Error?, database:DatabaseReference) in
                  if let error = error {
                    print("Data could not be saved: \(error).")
                    self.operationStatus = "userDoesntExist"
                  } else {
                    print("Data saved successfully!")
                    self.operationStatus = "userAdded"
                  }
                requestGroup.leave()
            }
            
            /*
             Fetch image data from the Firebase's storage if it is present.
             */
            requestGroup.enter()
            storage.child("images/\(contactID).jpg").getData(maxSize: 5 * 1024 * 1024) { (data, error) in
                guard error == nil else {
                    requestGroup.leave()
                    print("ERROR: IMAGE NOT FOUND")
                    return
                }
                
                foundProfilePic =  Image(uiImage: UIImage(data: data!)!)
                print("IMAGE WAS FOUND")
                requestGroup.leave()
            }
            
            requestGroup.leave()
        }
        
        /*
         After all async functions complete, notify the view to display the results
         */
        requestGroup.notify(queue: .main) {
            self.showResults = true
        }

    }
    
    /*
     If the contact is present locally.
     */
    var contactExistsResult: AnyView {
        let existingContact: Contact = userData.userContactsDictionary[barcode]!
        let squareProfileImage = existingContact.profilePic
            .resizable()
            .imageScale(.large)
            .aspectRatio(contentMode: .fit)
            .frame(width: 200)
            .cornerRadius(15)
            .overlay(RoundedRectangle(cornerRadius: 15)
                        .stroke(Color.white, lineWidth: 3))
            .padding(.bottom, 80)
            
        
        let roundProfileImage = existingContact.profilePic
            .resizable()
            .imageScale(.large)
            .aspectRatio(contentMode: .fit)
            .frame(width: 200)
            .cornerRadius(15)
            .foregroundColor(Color.white)
            .padding(.bottom, 80)
        
        return AnyView(
            VStack {
                if existingContact.profilePic == Image(systemName: "person.circle") {
                    roundProfileImage
                }
                else{
                    squareProfileImage
                }
                    
                Text("\(existingContact.name.isEmpty ? "User" : existingContact.name) is already in your contacts!")
                    .fixedSize(horizontal: false, vertical: true)   // Allow lines to wrap around
                    .multilineTextAlignment(.center)
                    .padding()
                    .foregroundColor(Color(UIColor.darkGray))
                    .background(Color.white)
                    .cornerRadius(15)
                Button(action: {
                    barcode = ""
                    self.operationStatus = ""
                    self.showResults = false
                }) {
                    Text("OK")
                        .frame(width: 150)
                }.padding(10)
                .background(Color.white)
                .cornerRadius(15)

            }
            .frame(width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
                .background(LinearGradient(gradient: Gradient(colors:
                                                                            [.red, .purple]),
                                                       startPoint: .top,
                                                       endPoint: .bottomTrailing))     // Ivory color
        )
    }
    
    /*
     If user was added successfully
     */
    var userAddedResult: AnyView {
        
        let squareProfileImage = self.foundProfilePic
            .resizable()
            .imageScale(.large)
            .aspectRatio(contentMode: .fit)
            .frame(width: 200)
            .cornerRadius(15)
            .overlay(RoundedRectangle(cornerRadius: 15)
                        .stroke(Color.white, lineWidth: 3))
            .padding(.bottom, 80)
            
        
        let roundProfileImage = self.foundProfilePic
            .resizable()
            .imageScale(.large)
            .aspectRatio(contentMode: .fit)
            .frame(width: 200)
            .cornerRadius(15)
            .foregroundColor(Color.white)
            .padding(.bottom, 80)
        
        return AnyView(
            VStack {
                /*
                 select profile image depending on whether the user's profile image is defined
                 */
                if self.foundProfilePic == Image(systemName: "person.circle") {
                    roundProfileImage
                }
                else{
                    squareProfileImage
                }
                Text("\(self.foundName) was added to your contacts!")
                    .fixedSize(horizontal: false, vertical: true)   // Allow lines to wrap around
                    .multilineTextAlignment(.center)
                    .padding()
                    .foregroundColor(Color(UIColor.darkGray))
                    .background(Color.white)
                    .cornerRadius(15)
                Button(action: {
                    barcode = ""
                    self.operationStatus = ""
                    self.showResults = false
                }) {
                    Text("OK")
                        .frame(width: 150)
                }.padding(10)
                .background(Color.white)
                .cornerRadius(15)

            }
            .frame(width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
                .background(LinearGradient(gradient: Gradient(colors:
                                                                            [.red, .purple]),
                                                       startPoint: .top,
                                                       endPoint: .bottomTrailing))     // Ivory color
        )
    }
    
    /*
     If user does not exist in the database.
     */
    var userDoesntExistResult: AnyView {
        return AnyView(
            VStack {
                Image(systemName: "exclamationmark.triangle")
                    .resizable()
                    .imageScale(.large)
                    .aspectRatio(contentMode: .fit)
                    .frame(width: 150)
                    .foregroundColor(Color.white)
                    .padding(.bottom, 80)
                Text("Uh Oh!\n That user does not exist!")
                    .fixedSize(horizontal: false, vertical: true)   // Allow lines to wrap around
                    .multilineTextAlignment(.center)
                    .padding()
                    .foregroundColor(Color(UIColor.darkGray))
                    .background(Color.white)
                    .cornerRadius(15)
                Button(action: {
                    barcode = ""
                    self.operationStatus = ""
                    self.showResults = false
                }) {
                    Text("OK")
                        .frame(width: 150)
                }.padding(10)
                .background(Color.white)
                .cornerRadius(15)
                

            }
            .frame(width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
                .background(LinearGradient(gradient: Gradient(colors:
                                                                            [.red, .purple]),
                                                       startPoint: .top,
                                                       endPoint: .bottomTrailing))
        )
        
    }
    
    /*
     If the QR code contained characters which are banned from bein in Firebase's database keys.
     */
    var invalidQRCodeResult: AnyView {
        return AnyView(
            VStack {
                Image(systemName: "exclamationmark.triangle")
                    .resizable()
                    .imageScale(.large)
                    .aspectRatio(contentMode: .fit)
                    .frame(width: 150)
                    .foregroundColor(Color.white)
                    .padding(.bottom, 80)
                Text("Uh Oh!\n That QR code doesn't belong to a Skilla user")
                    .fixedSize(horizontal: false, vertical: true)   // Allow lines to wrap around
                    .multilineTextAlignment(.center)
                    .padding()
                    .foregroundColor(Color(UIColor.darkGray))
                    .background(Color.white)
                    .cornerRadius(15)
                Button(action: {
                    barcode = ""
                    self.operationStatus = ""
                    self.showResults = false
                }) {
                    Text("OK")
                        .frame(width: 150)
                }.padding(10)
                .background(Color.white)
                .cornerRadius(15)
                

            }
            .frame(width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
                .background(LinearGradient(gradient: Gradient(colors:
                                                                            [.red, .purple]),
                                                       startPoint: .top,
                                                       endPoint: .bottomTrailing))
        )
        
    }
}
