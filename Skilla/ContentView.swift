//
//  ContentView.swift
//  Skilla
//
//  Created by Pau Lleonart Calvo on 12/3/20.
//

import FirebaseDatabase
import SwiftUI

struct ContentView : View {
    
    // Subscribe to changes in UserData
    @EnvironmentObject var userData: UserData
    
    var body: some View {
        if userData.userAuthenticated {
            return AnyView(MainView())
        } else {
            return AnyView(LoginView())
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
