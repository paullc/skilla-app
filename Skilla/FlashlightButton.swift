//
//  FlashlightButton.swift
//  Skilla
//
//  Created by Pau Lleonart Calvo on 12/6/20.
//  Copyright © 2020 SkillaTeam. All rights reserved.
//

import SwiftUI
import AVFoundation
 
struct FlashlightButton: View {
 
    // Input Parameter passed by reference
    @Binding var lightOn: Bool
    
    let color1 = LinearGradient(gradient: Gradient(colors:
                                                [.red, .purple]),
                           startPoint: .top,
                           endPoint: .bottomTrailing)
    let color2 = LinearGradient(gradient: Gradient(colors:
                                                    [.purple, .red]),
                           startPoint: .top,
                           endPoint: .bottomTrailing)
   
    var body: some View {
        Button(action: {
            withAnimation(.spring()) {
                self.toggleLight()
            }
        }) {
            Image(systemName: (lightOn ? "bolt.circle.fill" : "bolt.circle.fill"))
                .imageScale(.medium)
                .foregroundColor(self.lightOn ? .red: .purple)
                .font(Font.title.weight(.regular))
                .scaleEffect(self.lightOn ? 1.2 : 1.0)
                .rotationEffect(.degrees(self.lightOn ? 360: 0))
                
        }
    }
   
    func toggleLight() {
        guard let device = AVCaptureDevice.default(for: .video) else { return }
        self.lightOn.toggle()
       
        if device.hasTorch {
            do {
                try device.lockForConfiguration()
                device.torchMode = (self.lightOn) ? .on : .off
                device.unlockForConfiguration()
            } catch {
                print("Unable to Activate Flashlight!")
            }
        } else {
            print("Flashlight is Unavailable!")
        }
    }
}
