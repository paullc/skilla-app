//
//  MainView.swift
//  Skilla
//
//  Created by Pau Lleonart Calvo on 12/3/20.
//  Copyright © 2020 SkillaTeam. All rights reserved.
//

import SwiftUI

struct MainView: View {
    
    var body: some View {
        TabView {
            JobList()
                .tabItem {
                    Image(systemName: "briefcase")
                    Text("Jobs")
                }
            ContactsList()
                .tabItem {
                    Image(systemName: "rectangle.stack.person.crop")
                    Text("Contacts")
                }
            Connect()
                .tabItem {
                    Image(systemName: "person.crop.circle.badge.plus")
                    Text("Connect")
                }
            Profile()
                .tabItem {
                    Image(systemName: "person")
                    Text("Profile")
                }
            
        }   // End of TabView
        .font(.headline)
        .imageScale(.large)
        .font(Font.title.weight(.regular))
    }
    
}

struct MainView_Previews: PreviewProvider {
    static var previews: some View {
        MainView()
    }
}
