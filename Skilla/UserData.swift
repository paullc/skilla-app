//
//  UserData.swift
//  Skilla
//
//  Created by Pau Lleonart Calvo on 12/3/20.
//  Copyright © 2020 SkillaTeam. All rights reserved.
//

import Combine
import SwiftUI
import FirebaseDatabase
import FirebaseStorage

let storage = Storage.storage().reference()
let database = Database.database().reference()

final class UserData: ObservableObject {
    
    // Publish if the user .is authenticated or not
    @Published var userAuthenticated = false
    
    // Publish if biometric authentication methods are not available
    @Published var listOfJobs = jobsList
    
    // Publish if there is a new user
    @Published var newUser = false
    
    @Published var userContactsDictionary = contactStructDictionary
    
    @Published var userContactInformation = userContact
    
}

